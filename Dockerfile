FROM ubuntu:19.10 AS install_deps

# Install system packages
RUN apt-get --yes -qq update \
 && apt-get --yes -qq upgrade \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get --yes -qq install \
      curl \
      gnupg2 \
# Install MKL from Intel
 && curl -LOs https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB \
 && apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB \
 && echo "deb https://apt.repos.intel.com/mkl all main" > /etc/apt/sources.list.d/intel-mkl.list \
 && apt-get --yes -qq update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get --yes -qq install \
      build-essential \
      g++ \
      gcc \
      gfortran \
      git \
      intel-mkl-64bit-2020.0-088 \
      libboost-all-dev \
      python3 \
      zlib1g-dev \
 && (find /opt/intel -name "ia32*" -exec rm -rf {} \; || echo "removing ia32 binaries") ; \
    (find /opt/intel -name "examples" -type d -exec rm -rf {} \; || echo "removing examples") ; \
    (find /opt/intel -name "benchmarks" -exec rm -rf {} \; || echo "removing benchmarks") ; \
    (find /opt/intel -name "documentation*" -exec rm -rf {} \; || echo "removing documentation") ; \
    (rm -rf /opt/intel/mkl/interfaces ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*.a ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*mpi*.so ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*tbb*.so ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*pgi*.so ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*mc*.so ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*blacs*.so ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*scalapack*.so ) ; \
    (rm -rf /opt/intel/mkl/lib/intel64/*mic*.so ) ; \
    apt-get clean autoclean \
 && apt-get autoremove -y \
 && rm -rf /usr/share/doc \
 && apt-get --yes -qq clean \
 && rm -rf /var/lib/apt/lists/* \
# Install newer version of CMake
 && mkdir -p /opt/cmake \
 && curl -Ls https://cmake.org/files/v3.14/cmake-3.14.7-Linux-x86_64.tar.gz | tar -xz -C /opt/cmake --strip-components=1

# LSDALTON
FROM install_deps AS install_lsdalton

SHELL ["/bin/bash", "-c"]

RUN source /opt/intel/mkl/bin/mklvars.sh intel64 \
 && git clone --single-branch --branch master --recursive https://gitlab.com/dalton/lsdalton.git \
 && cd lsdalton \
# Configure LSDALTON
 && python3 setup \
      --cmake=/opt/cmake/bin/cmake \
      --fc=gfortran \
      --cc=gcc \
      --cxx=g++ \
      --omp \
      --type=release \
      --prefix=/opt/lsdalton \
      #-DENABLE_RSP=ON \
      #-DENABLE_XCFUN=ON \
      #-DENABLE_OPENRSP=ON \
      #-DENABLE_PCMSOLVER=ON \
# Build and install LSDALTON
 && /opt/cmake/bin/cmake --build build --target install -- VERBOSE=1

FROM install_lsdalton AS lsdalton

RUN apt-get --yes -qq update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get --yes -qq remove --purge \
      curl \
      gnupg2 \
      g++ \
      git \
      libboost-all-dev \
      python3 \
 && apt-get clean autoclean \
 && apt-get autoremove -y --purge \
 && rm -rf /usr/share/doc \
 && apt-get --yes -qq clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /lsdalton /opt/cmake

FROM lsdalton

COPY --from=lsdalton /opt/lsdalton /opt/lsdalton/

ENV PATH="/opt/lsdalton/bin:${PATH}"
