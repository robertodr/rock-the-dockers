{ system ? builtins.currentSystem }:

let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs { inherit system; };

  callPackage = pkgs.lib.callPackageWith pkgs;

  lsdalton = callPackage ./pkgs/lsdalton { };

  dockerImage = pkg:
    pkgs.dockerTools.buildImage {
      name = "registry.gitlab.com/robertodr/rock-the-dockers";
      tag = pkg.version;
      created = "now";
      contents = [
        pkg
        pkgs.busybox
      ];
      config = {
        Volumes = {
          "/scratch" = {};
        };
      };
    };

in dockerImage lsdalton
