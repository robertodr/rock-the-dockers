{
  boost,
  cmake,
  fetchgit,
  gcc,
  gfortran,
  libffi,
  mkl,
  python3,
  stdenv,
  zlib,
  pylsd ? false,
  mpi ? null
}:
let
  inherit (stdenv.lib) optional optionals;
in
stdenv.mkDerivation rec {
  version = "2020.1";
  pname = "LSDALTON";
  src = fetchgit {
    url = "https://gitlab.com/dalton/lsdalton.git";
    rev = "fe111af057c390d56525bdc09856d450b2fefb3f";
    sha256 = "1mvbbvnh95vnw3rma6r0c4jvz0z19xv2a3w28w6n24r4vfzi1kxm";
    leaveDotGit = false;
  };

  passthru = {
    mpiSupport = (mpi != null);
    inherit mpi;
  };

  nativeBuildInputs = [
  ];

  buildInputs = [
    boost
    cmake
    gcc
    gfortran
  ];

  propagatedBuildInputs = [
    mkl
    python3
    zlib
  ]
    ++ optional (mpi != null) mpi
    ++ optional pylsd [
      libffi
      python3.pkgs.cffi
      python3.pkgs.numpy.override {
        blas = mkl;
      }
      python3.pkgs.scipy.override {
        blas = mkl;
      }
    ]
  ;

  preConfigure = ''
    export MATH_ROOT="${mkl.outPath}"
  '';

  cmakeFlags = [
    "-DENABLE_RSP=ON"
    "-DENABLE_XCFUN=ON"
    "-DENABLE_PCMSOLVER=ON"
    "-DENABLE_OPENRSP=ON"
    "-DENABLE_OPENMP=ON"
    # These shouldn't be needed...
    "-DCMAKE_INSTALL_LIBDIR=lib"
    "-DCMAKE_INSTALL_BINDIR=bin"
    "-DCMAKE_INSTALL_INCLUDEDIR=include"
  ]
    ++ optional pylsd "-DENABLE_PYTHON_INTERFACE=ON"
    ++ optional (mpi != null) [
      "-DENABLE_MPI=ON"
      "-DCMAKE_Fortran_COMPILER=${mpi}/bin/mpifort"
      "-DCMAKE_C_COMPILER=${mpi}/bin/mpicc"
      "-DCMAKE_CXX_COMPILER=${mpi}/bin/mpic++"
      ]
  ;

  meta = with stdenv.lib; {
    inherit version;
    description = "LSDALTON";
    homepage = https://daltonproject.org;
    license = licenses.lgpl21;
    platforms = platforms.all;
    maintainers = with maintainers; [ robertodr ];
  };
}
